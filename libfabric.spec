Name:           libfabric
Version:        1.22.0
Release:        1
Summary:        A core component of OpenFabric Interfaces
License:        BSD-2-Clause OR GPL-2.0-only
URL:            https://github.com/ofiwg/libfabric
Source0:        https://github.com/ofiwg/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.bz2

BuildRequires:  gcc libibverbs-devel libnl3-devel librdmacm-devel chrpath make
%ifarch x86_64
BuildRequires:  infinipath-psm-devel libpsm2-devel
%endif

%description
OpenFabrics Interfaces (OFI) is a framework focused on exporting fabric
communication services to applications.  OFI is best described as a collection
of libraries and applications used to export fabric services.  The key
components of OFI are: application interfaces, provider libraries, kernel
services, daemons, and test applications.

Libfabric is a core component of OFI.  It is the library that defines and
exports the user-space API of OFI, and is typically the only software that
applications deal with directly.  It works in conjunction with provider
libraries, which are often integrated directly into libfabric.


%package        devel
Summary:        Including header files and libraries for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
This contains header files and libraries for the developing applications that
use libfabric.


%package help
Summary:        Man files for libfabric
Requires:       man
BuildArch:      noarch

%description help
This contains man files for the using of libfabric.

%prep
%autosetup -n libfabric-%{version} -p1


%build
%configure --disable-silent-rules --disable-static
%make_build  V=1


%install
%make_install
%delete_la


%ldconfig_scriptlets
chrpath -d %{buildroot}%{_bindir}/fi_*


%files
%license COPYING
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%doc AUTHORS README
%{_includedir}/rdma/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%files help
%{_mandir}/man*/*


%changelog
* Mon Dec 02 2024 xu_ping <707078654@qq.com.com> - 1.22.0-1
- update to 1.22.0

* Fri Dec 29 2023 liyanan <liyanan61@h-partners.com> - 1.20.0-1
- update to 1.20.0

* Wed May 17 2023 liyanan <thistleslyn@163.com> - 1.18.0-1
- update to 1.18.0

* Sat Nov 12 2022 hua <dchang@zhixundn.com> 1.16.1-1
- update to 1.16.1

* Fri Sep 10 2021 gongzhengtang <gongzhengtang@huawei.com> -1.6.1-5
- fix rpath

* Wed Aug 04 2021 Liu Yinsi <liuyinsi@163.com> - 1.6.1-4
- fix multiple definition to support gcc 10

* Fri Nov 29 2019 huyan <hu.huyan@huawei.com> - 1.6.1-3
- Package Initialization
